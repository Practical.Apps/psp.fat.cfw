**PSP 1000 PRO CFW INSTALLATION INSTRUCTIONS**

**Summary**\
This repository has all the files and instruction necessary to install PRO-C2 custom firmware on an original PSP (The 1000 or FAT version).

**Why**
   - Custom firmware allows your PSP to run custom software (homebrew) as well as unsigned copies of 
games (such as those you might find offered for free download on a ROM sharing site).
   - Play games from other systems via emulation, including PS1 games on the PSP's built-in PS1 emulator.
   - Increase performance of PSP games by playing from an SD card rather than a UMD.
   - Breathe new life into an old, unused piece of tech. 

**Materials Required**
- *PSP 1000*, hopefully you already have one
   - If you don't I'd frankly suggest getting a newer model (2000 or 3000 series) rather than purchase a 1000 version
- *ProDuo memory card* (Suggested: MicroSD to ProDuo adapter)
   - A large capacity MicroSD in an adapter is far preferable to any old ProDuo card
- *Transfer mechanism* to get data onto the memory card. Either:
   - A card reader that supports MicroSD or Pro2Duo or
   - USB (Type A) to USB (Mini) cable
<p>
<details>
<summary>Notes on USB connection (Click to expand):</summary>

With this cable you can connect through the USB (Mini) port on the top of the PSP.\
There are USB cables available with a forked end, with one branch for USB (Mini) and another for the barrel type power connection. 
Since PSPs and USB are both 5V this is strongly recommended over buying a separate charger. Unfortunately the 1000 series PSPs do not have the ability to charge via the USB port that is a convenient feature of later versions and many modern electronics.\
*Also of note*:\
Connection via the USB port on older firmware versions has to be started manually.
On the PSP you need to go to the `Settings` menu and select `USB Connection`. 
This becomes automatic (starts after plugging in the USB cable) on newer firmware versions.\
**__You will not be able to do anything else on the PSP while it is connected for USB file transfer.__**

</details>
</p>

- *New battery*
   - While this isn't absolutely required, any old PSP battery is likely dead by now, and the PSP was meant to be a wireless portable gaming system.


**Preparation**

*Format*\
If you are using any fresh card it will need to be formatted. This can be done on the PSP
By navigating the menus to (`Settings > System Settings > Format Memory Stick`)
This will ensure the card is in the proper format and has all the necessary directories.

*Check current firmware version*\
Chances are good that the firmware has not been updated in a long time.\
To check the version number, navigate to (`Settings > System Settings > System Info`)

![IMAGE1](IMAGES/1.png "System Information Screen [OLD]")

As you can see, my old PSP was still running firmware version 2.82

*Update to most recent firmware version*\
It may not be absolutely necessary, but installing a custom firmware version that does not match
your PSP's current firmware version can brick your system. Considering the risk, it's suggested
that you update to the most recent official firmware first, v.6.61.

<p>
<details>
<summary>Note on WiFi (Click to expand):</summary>

You can still download official updates on your PSP via WiFi and the built in `Network Update` feature.
However, you may have difficulty connecting to WiFi with an old firmware version.\
The PSP does not support WPA2 but it may still fail trying to connect to a network offering WPA and WPA2
simultaneously. If this happens to you, and you still want to use the onboard WiFi, try creating a guest
network with only WPA encryption. Take care to match encryption settings between your WiFi network and
your PSP network settings. They should both be TKIP or both AES. Not mixed.\
You can enable wireless isolation on the guest network if this is a security concern.

</details>
</p>

Regardless of whether you would like get the official firmware straight to your PSP from Sony or
download it from this repository and transfer it manually, the end result will be the same. 
You will get the `UPDATE` folder into your PSP's `GAME` directory. The built-in `Network Update`
process will make this mostly automatic. For those installing manually, copy the `UPDATE` folder\
from: `This repository > FIRMWARE > OFW_661`\
to: `PSP memory card > PSP > GAME`\
When you select `Memory Stick` from the `Game` menu on your PSP, you should see an entry for
`PSP Update ver 6.61` that can be selected and launched just like any other game.
The update should not take very long. When it's done, your PSP should restart automatically.
Check your firmware version again to confirm the result, though you will likely notice minor
changes to menu items before you even make it there.

Reference: https://revive.today/psp/firmware/ \
https://www.wikihow.com/Upgrade-Your-PSP-Firmware

**Install the custom firmware**\
Finally we will install the custom firmware, PRO-C2 by Team PRO. PRO-C2 is not the only custom firmware 
available, but it is arguably the best. To prepare the installation, transfer the contents\
from: `This repository > FIRMWARE > CFW_PROC2`\
to: `PSP memory card`\
That is, copy the new `PSP` and `seplugins` folders to the root directory of the card. If your card is 
properly formatted there should already be a `PSP` folder in the root directory. As such you should select 
the option to "Merge" or "Combine" the new with the existing folder when copying.\
The result will be three new entries in the Game folder: `Pro Update`, `Pro CIPL Flasher` and `Pro Fast Recovery`.

To update to the custom firmware, simply launch `Pro Update` from the `Game > Memory Stick` menu as you did with the official update.\
Once open, you will be prompted with a text only interface that looks like this:

![IMAGE2](IMAGES/2.png "PRO Update Text Interface")

Press the X button to proceed with the installation. When it has completed you will be prompted to press X again and the PSP will restart.\
That was it, you can check the system version information again to confirm that the change has been made.

![IMAGE3](IMAGES/3.png "System Information Screen [NEW]")

At this stage in the process you have all the functionality of custom firmware but it is not permanent. 
If you restart your PSP you will be back on the official firmware. You will have to re-run the `Pro Update` 
program to get the custom firmware back. To make the changes permanent, all you need to do is run the 
`CIPL Flasher` program. When you launch it from the Game menu you will be on another text only interface, 
which looks like this:

![IMAGE4](IMAGES/4.png "System Information Screen [OLD]")

Pressing X to confirm on this screen will write your new custom firmware to the internal storage 
on the PSP. Once it completes the restart process, you are completely finished. You can go onto 
your card and remove all of the entries in the `GAMES` directory that were made during the install, 
including the `UPDATE` folder with the official firmware in it.

Reference: https://revive.today/psp/cfw/pro/

**What next**\
The first thing I would recommend doing after finishing your install would be to set up a plugin
called Categorieslite. The idea of the plugin is to help sort your programs on your PSP by giving
you a menu for your various subdirectories when you select your memory stick in the Game menu.\
To install it just transfer all the contents\
from: `This repository > PLUGINS`\
to: `PSP memory card > seplugins`

Without getting into too much detail, the `categorieslite` folder contains the plugin code, while the
line in the `VSH.txt` file enables it. This is how most plugins are installed. I've included the 
GAME and POPS files even though they are empty for your future convenience. If you install plugins
later which affect PSP games or PSX games, they must be declared and enabled in the same way in
either the `GAME.txt` or `POPS.txt` files respectively. To finish the installation, you will have to open the 
recovery menu by turning the PSP all the way off (**hold** the power switch up) and then power on while holding down the 
right bumper (R). Once in the menu just select `Reset VSH` and you will boot into the home screen with your new plugin enabled.

![IMAGE5](IMAGES/5.png "PRO Recovery Menu")

With categorieslite enabled, you can separate games and programs into different directories and sort them.\
My current set up is as follows:

/ISO/
- CAT_01PSP

/PSP/GAME/
- CAT_02PSX
- CAT_03Emulators
- CAT_04Homebrew

To make this setup work you will have to go into your `Settings` menu and change the new menu entries that were made when you 
added the categorieslite plugin. Go to `Settings > System Settings` on your PSP and make sure the settings are as follows:\
`Category mode`: Contextual menu\
`Category prefix`: Use CAT prefix\
`Show uncategorized`: No\
`Sort categories`: Yes

![IMAGE6](IMAGES/6.png "Custom Categorieslite Menu")

You can then sort games/programs into their corresponding folders and the end result is much cleaner
and more organized than viewing everything in one large cluttered list.

Reference: https://www.reddit.com/r/PSP/wiki/categorieslite \
https://www.reddit.com/r/PSP/wiki/plugins

**Installing Games**\
Game ROMs can be installed very easily. PSP games will come as .iso files and should be kept in the `ISO` folder. 
PS1 games will come as an EBOOT.PBP file and should be kept in the `/PSP/GAME` folder.
It's important to note that these EBOOT files need their own folders otherwise they will show up as "corrupted data." 
Many choose to use the game's UID as a folder name and they may come that way in a download but you *can* give the folders human-readable names if you want, as long as they don't contain any strange characters.

PSP example path: X:`/ISO/CAT_01PSP/Tetris.iso` \
PS1 example path: X:`/PSP/GAME/CAT_02PSX/Tekken/EBOOT.PBP`

If a PS1 game does not show up as "corrupted data" in the menu but also doesn't run, there's a chance that it is not
compatible with the POPS version that your PSP is trying to run it with. Here is a guide on how to address that:\
POPSLoader guide: https://nblog.org/guides/how-to-use-popsloader-guide/

If you have a PS1 game as an ISO but can't find an EBOOT version, you can convert it yourself:\
PS1 ISOs to EBOOT: https://www.instructables.com/How-to-make-PS1-games-change-to-eboots-for-PSP-usa/

*Emulators*\
Emulators for other systems are available for the PSP. The typical install procedure is just to
extract the program folder to the `/PSP/GAME` folder just like PS1 games. This is why it's convenient
to have a separate folder for them. ROMs for emulated games are then kept within the emulator's folder. 
These are a few of the emulators I use:

Masterboy 2.10 (GB/GBC)\
https://www.psx-place.com/resources/gbc-master-boy-2-10.232/download?version=260 \
uo gpSP Kai v3.4 build 230 (GBA)\
https://www.mediafire.com/file/8o4400b1j9ci5p7/gpSP_Kai_v3.4_test4-b230%2528fat%2529.zip/file \
DaedalusX64 1.18 (N64)\
https://github.com/DaedalusX64/daedalus/releases/tag/1.1.8

I can confirm that these work for the PSP 1000, but there is no guarantee they will work for other
versions. This gpSP version will definitely not work on anything other than PSP 10XX version so look elsewhere for that.

*Other suggested apps*\
https://revive.today/psp/apps/ \
With some work it is possible to play online multiplayer with other PSPs, even a PSP emulator \
XLink Kai site: https://www.teamxlink.co.uk/ \
Guiide for PSP multiplayer: https://forums.ppsspp.org/showthread.php?tid=8349
